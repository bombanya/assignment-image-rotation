#include "image.h"
#include <malloc.h>

struct image_with_status image_create(uint64_t width, uint64_t height){
    struct image img = {.width = width, .height = height,
            .data = malloc(sizeof(struct pixel) * width * height)};
    if (width * height != 0 && img.data == NULL) return (struct image_with_status) {.status = MEMORY_ERROR};
    else return (struct image_with_status) {.status = OK, .img = img};
}

void image_delete(struct image img){
    free(img.data);
}

struct pixel* image_coords_to_addr(struct image img, uint64_t row, uint64_t column){
    return img.data + (img.width * row + column);
}
