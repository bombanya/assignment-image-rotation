#include "transformations.h"

struct image_with_status image_rotate_cntr_clck_90( struct image const source ){
    struct image_with_status rotated = image_create(source.height, source.width);
    if (rotated.status != OK) return rotated;

    for (uint64_t i = 0; i < source.height; i++){
        for (uint64_t j = 0; j < source.width; j++){
            *image_coords_to_addr(rotated.img, rotated.img.height- j - 1, i)
            = *image_coords_to_addr(source, i, j);
        }
    }

    return rotated;
}
