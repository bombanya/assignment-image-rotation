#include "bmp_helpers.h"

#define BMP_FILE_SIGNATURE 0x4d42
#define BITMAP_INFO_HEADER 40
#define NO_COMPRESSION 0

struct __attribute__((packed)) bmp_header{
    uint16_t signature;
    uint32_t file_size;
    uint32_t reserved;
    uint32_t data_offset;
    uint32_t header_size;
    uint32_t width;
    uint32_t height;
    uint16_t planes;
    uint16_t bits_per_pixel;
    uint32_t compression;
    uint32_t image_size;
    uint32_t x_pixels_per_m;
    uint32_t y_pixels_per_m;
    uint32_t colors_used;
    uint32_t important_colors;
};

static const char* const BMP_READ_ERR_MSGS[] = {
        [READ_INVALID_SIGNATURE] = "invalid signature",
        [READ_INVALID_BITS] = "invalid bits per pixel",
        [READ_INVALID_HEADER] = "invalid bmp header type",
        [READ_HEADER_READING_ERROR] = "error while reading header",
        [READ_PIXELS_READING_ERROR] = "error while reading pixels",
        [READ_COMPRESSION_ERROR] = "support only uncompressed bmps",
        [READ_MEMORY_ERROR] = "memory error"
};

static const char* const BMP_WRITE_ERR_MSGS[] = {
        [WRITE_ERROR] = "error while writing bmp"
};

static uint8_t padding_calc(uint32_t width);

static uint64_t file_size_calc(uint64_t width, uint64_t height);

static enum bmp_read_status header_verify(struct bmp_header header);
static enum bmp_read_status pixels_read(FILE* in, struct image* img);

static struct bmp_header header_create(struct image const img);
static enum bmp_write_status pixels_write(FILE* out, struct image const img);

enum bmp_read_status image_from_bmp( FILE* in, struct image* img ){
    struct bmp_header header;
    if (!fread(&header, sizeof(struct bmp_header), 1, in)) return READ_HEADER_READING_ERROR;

    enum bmp_read_status header_status = header_verify(header);
    if (header_status != READ_OK) return header_status;

    struct image_with_status img_struct = image_create(header.width, header.height);
    if (img_struct.status != OK) return READ_MEMORY_ERROR;
    *img = img_struct.img;
    if (fseek(in, header.data_offset, SEEK_SET) != 0) return READ_PIXELS_READING_ERROR;
    return pixels_read(in, img);
}

enum bmp_write_status image_to_bmp( FILE* out, struct image const img) {
    struct bmp_header header = header_create(img);
    if (!fwrite(&header, sizeof(struct bmp_header), 1, out)) return WRITE_ERROR;
    return pixels_write(out, img);
}

bool image_from_bmp_with_err_output(FILE* in, struct image* img){
    enum bmp_read_status result = image_from_bmp(in, img);
    if (result == READ_OK) return true;
    else{
        fprintf(stderr, "%s\n", BMP_READ_ERR_MSGS[result]);
        return false;
    }
}

bool image_to_bmp_with_err_output(FILE* out, struct image const img){
    enum bmp_write_status result = image_to_bmp(out, img);
    if (result == WRITE_OK) return true;
    else{
        fprintf(stderr, "%s\n", BMP_WRITE_ERR_MSGS[result]);
        return false;
    }
}

static uint8_t padding_calc(uint32_t width){
    return (4 - (sizeof(struct pixel) * width) % 4) % 4;
}

static uint64_t file_size_calc(uint64_t width, uint64_t height){
    return sizeof(struct bmp_header)
           + ((sizeof(struct pixel) * width + padding_calc(width)) * height);
}

static enum bmp_read_status header_verify(struct bmp_header header){
    if (header.signature != BMP_FILE_SIGNATURE) return READ_INVALID_SIGNATURE;
    if (header.header_size != BITMAP_INFO_HEADER) return READ_INVALID_HEADER;
    if (header.bits_per_pixel != 24) return READ_INVALID_BITS;
    if (header.compression != NO_COMPRESSION) return READ_COMPRESSION_ERROR;
    return READ_OK;
}

static struct bmp_header header_create(struct image const img){
    return (struct bmp_header) {
            .signature = BMP_FILE_SIGNATURE,
            .file_size = file_size_calc(img.width, img.height),
            .reserved = 0,
            .data_offset = sizeof(struct bmp_header),
            .header_size = BITMAP_INFO_HEADER,
            .width = img.width,
            .height = img.height,
            .planes = 1,
            .bits_per_pixel = 24,
            .compression = NO_COMPRESSION,
            .image_size = 0,
            .x_pixels_per_m = 0,
            .y_pixels_per_m = 0,
            .colors_used = 0,
            .important_colors = 0
    };
}

static enum bmp_read_status pixels_read(FILE* in, struct image* img){
    uint8_t padding = padding_calc(img->width);
    for (uint32_t i = 0; i < img->height; i++) {
        if (fread(image_coords_to_addr(*img, img->height - i - 1, 0),   //bmp stores rows from bottom
                  sizeof(struct pixel),img->width, in) == img->width){        //to top, therefore, save them
            if (fseek(in, padding, SEEK_CUR) == 0) continue;               // in reverse order
        }
        image_delete(*img);
        return READ_PIXELS_READING_ERROR;
    }
    return READ_OK;
}

static enum bmp_write_status pixels_write(FILE* out, struct image const img){
    uint8_t padding = padding_calc(img.width);
    uint32_t padding_zeros = 0;
    for (uint64_t i = 0; i < img.height; i++){
        if (fwrite(image_coords_to_addr(img, img.height - i - 1, 0),   //again, reverse order
                   sizeof(struct pixel), img.width, out) != img.width)
            return WRITE_ERROR;
        if (fwrite(&padding_zeros, 1, padding, out) != padding) return WRITE_ERROR;
    }
    return WRITE_OK;
}
