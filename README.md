# Assignment: Image rotation
---
Лабораторная работа: Поворот картинки

# Файлики
- `bmp_helpers` - функции для чтения и записи bmp-картинок
- `image` - структуры и функции для работы с внутренним представлением картинок
- `file_utils` - функции для работы с файлами с выводом ошибок
- `transformations` - всякие издевательства над картинками

# Usage

```
./image-transformer <source-image> <transformed-image>
```
