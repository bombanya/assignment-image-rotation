#ifndef LL_LABA3_BMP_HELPERS_H
#define LL_LABA3_BMP_HELPERS_H

#include <stdio.h>
#include <stdbool.h>
#include "image.h"

enum bmp_read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_HEADER_READING_ERROR,
    READ_PIXELS_READING_ERROR,
    READ_COMPRESSION_ERROR,
    READ_MEMORY_ERROR
};

enum bmp_write_status {
    WRITE_OK = 0,
    WRITE_ERROR
};

enum bmp_read_status image_from_bmp( FILE* in, struct image* img );
enum bmp_write_status image_to_bmp( FILE* out, struct image const img );

bool image_from_bmp_with_err_output(FILE* in, struct image* img);
bool image_to_bmp_with_err_output(FILE* out, struct image const img);

#endif //LL_LABA3_BMP_HELPERS_H
