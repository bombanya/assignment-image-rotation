#ifndef LL_LABA3_FILE_UTILS_H
#define LL_LABA3_FILE_UTILS_H

#include <stdio.h>
#include <stdbool.h>

FILE* fopen_with_err_output(const char* filename, const char* mode);
bool fclose_with_err_output(FILE* file);

#endif //LL_LABA3_FILE_UTILS_H
