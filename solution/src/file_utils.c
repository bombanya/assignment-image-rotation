#include "file_utils.h"
#include <errno.h>
#include <string.h>

FILE* fopen_with_err_output(const char* filename, const char* mode){
    FILE* file = fopen(filename, mode);
    if (!file){
        fprintf(stderr, "file open error: %d %s\n", errno, strerror(errno));
    }
    return file;
}

bool fclose_with_err_output(FILE* file){
    if (fclose(file) != 0){
        fprintf(stderr, "file close error: %d %s\n", errno, strerror(errno));
        return false;
    }
    return true;
}
