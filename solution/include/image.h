#ifndef LL_LABA3_IMAGE_H
#define LL_LABA3_IMAGE_H

#include <inttypes.h>

struct pixel {
    uint8_t b, g, r;
};

struct image {
    uint64_t width, height;
    struct pixel* data;  //stores rows of pixels from top to bottom
};

struct image_with_status{
    enum {OK, MEMORY_ERROR} status;
    struct image img;
};

struct image_with_status image_create(uint64_t width, uint64_t height);

void image_delete(struct image);

struct pixel* image_coords_to_addr(struct image img, uint64_t row, uint64_t column);

#endif //LL_LABA3_IMAGE_H
