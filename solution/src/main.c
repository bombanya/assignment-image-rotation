#include "bmp_helpers.h"
#include "file_utils.h"
#include "image.h"
#include "transformations.h"

void usage(){
    fprintf(stderr, "Usage: ./<program_name> <source_image> <transformed_image>");
}

int main(int argc, char** argv) {
    if (argc != 3) {
        usage();
        return 1;
    }

    FILE* input = fopen_with_err_output(argv[1], "rb");
    if (!input) return 1;

    struct image img;
    bool status = image_from_bmp_with_err_output(input, &img);

    if (!status) {
        fclose_with_err_output(input);
        return 1;
    }

    if (!fclose_with_err_output(input)){
        image_delete(img);
        return 1;
    }

    FILE* output = fopen_with_err_output(argv[2], "wb");
    if (!output) {
        image_delete(img);
        return 1;
    }

    struct image_with_status rotated_struct = image_rotate_cntr_clck_90(img);
    image_delete(img);
    if (rotated_struct.status != OK){
        fprintf(stderr, "memory error\n");
        fclose_with_err_output(output);
        return 1;
    }

    status = image_to_bmp_with_err_output(output, rotated_struct.img);
    image_delete(rotated_struct.img);
    status = fclose_with_err_output(output) ? status : false;

    if (status) return 0;
    else return 1;
}
