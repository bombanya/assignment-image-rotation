#ifndef LL_LABA3_TRANSFORMATIONS_H
#define LL_LABA3_TRANSFORMATIONS_H

#include "image.h"

struct image_with_status image_rotate_cntr_clck_90( struct image const source );

#endif //LL_LABA3_TRANSFORMATIONS_H
